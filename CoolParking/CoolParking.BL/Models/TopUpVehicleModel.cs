﻿namespace CoolParking.BL.Models
{
    public class TopUpVehicleModel
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}