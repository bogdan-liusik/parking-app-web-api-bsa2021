﻿namespace CoolParking.BL.Models
{
    public class VehicleModel
    {
        public string Id { get; set; }
        public VehicleType VehicleType { get; set; }
        public decimal Balance { get; set; }
    }
}