﻿using System;
using System.Linq;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        readonly string id;
        readonly VehicleType _vehicleType;
        static Random _random;
        const string idPattern = @"^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$";

        static Vehicle()
        {
            _random = new Random();
        }

        [JsonConstructor]
        public Vehicle()
        {

        }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            id = GenerateRandomRegistrationPlateNumber();
            _vehicleType = vehicleType;
            Balance = balance;
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance) : this(vehicleType, balance)
        {
            if (!IsValidateID(id))
            {
                throw new ArgumentException("ID must match the template XX-YYYY-XX (X letter, Y number)");
            }
            if ((int)vehicleType > 4 || (int)vehicleType < 1)
            {
                throw new ArgumentException("Invalid vehicle type!");
            }
            if(balance < 0)
            {
                throw new ArgumentException("Balance must be positive!");
            }
            this.id = id;
        }

        public string Id => id;

        public VehicleType VehicleType => _vehicleType;

        public decimal Balance { get; internal set; }
        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            return $"{RandomString(2)}-{_random.Next(0, 9999).ToString("D4")}-{RandomString(2)}";
        }

        public static bool IsValidateID(string id)
        {
            return Regex.IsMatch(id, idPattern);
        }

        private static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            return new string(Enumerable.Range(1, length)
                .Select(_ => chars[_random.Next(chars.Length)]).ToArray());
        }
    }
}