﻿using System;
using System.Globalization;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public TransactionInfo(string vehicleID, decimal amount) : this(DateTime.Now, vehicleID, amount) { }

        public TransactionInfo(DateTime transactionTime, string vehicleID, decimal amount)
        {
            TransactionTime = transactionTime;
            VehicleID = vehicleID;
            Sum = amount;
        }

        public DateTime TransactionTime { get; set; }

        public string VehicleID { get; set; }
        
        public decimal Sum { get; set; }

        public override string ToString()
        {
            var time = TransactionTime.ToString($@"M/dd/yyyy hh:mm:ss tt", new CultureInfo("en-US"));
            return String.Format($"{time}: {Sum} money withdrawn from vehicle with Id='{VehicleID}'.\n");
        }
    }
}