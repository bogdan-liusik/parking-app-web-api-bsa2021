﻿using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static readonly Lazy<Parking> lazy = new Lazy<Parking>(() => new Parking());

        private Parking()
        {
            Balance = 0;
            Vehicles = new List<Vehicle>();
        }

        public static Parking GetInstance => lazy.Value;

        public decimal Balance { get; set; }

        public List<Vehicle> Vehicles { get; set; }
    }
}