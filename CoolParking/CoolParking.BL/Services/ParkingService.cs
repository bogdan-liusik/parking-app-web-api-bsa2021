﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Timers;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        Parking _parking;
        List<TransactionInfo> transactions;
        ITimerService _withdrawTimer;
        ITimerService _logTimer;
        ILogService _logService;
        object parkingLocker = new object();

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.GetInstance;
            transactions = new List<TransactionInfo>();

            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;

            _withdrawTimer.Elapsed += DoTransaction;
            _logTimer.Elapsed += Log;

            _withdrawTimer.Start();
            _logTimer.Start();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            lock (parkingLocker)
            {
                if (_parking.Vehicles.Exists(v => v.Id == vehicle.Id))
                {
                    throw new ArgumentException("There are already exists car with such ID, try again!");
                }

                if (_parking.Vehicles.Count >= GetCapacity())
                {
                    throw new InvalidOperationException("There is no parking space available!");
                }

                if (vehicle.Balance < 0)
                {
                    throw new ArgumentException("You must refill your balance!");
                }

                _parking.Vehicles.Add(vehicle);
            }
        }

        public decimal GetBalance()
        {
            lock (parkingLocker)
            {
                return _parking.Balance;
            }
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            lock (parkingLocker)
            {
                return GetCapacity() - _parking.Vehicles.Count;
            }
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public decimal GetLastEarnedMoney()
        {
            return GetLastParkingTransactions().Sum(t => t.Sum);
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            lock (parkingLocker)
            {
                return _parking.Vehicles.AsReadOnly();
            }
        }

        public string ReadFromLog()
        {
            lock (parkingLocker)
            {
                return _logService.Read();
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            lock (parkingLocker)
            {
                if (!_parking.Vehicles.Exists(v => v.Id == vehicleId))
                {
                    throw new ArgumentException("No such vehicle in the parking!");
                }

                if (_parking.Vehicles.Find(v => v.Id == vehicleId).Balance < 0)
                {
                    throw new InvalidOperationException("You must refill your balance!");
                }

                _parking.Vehicles.Remove(_parking.Vehicles.Find(v => v.Id == vehicleId));
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            lock (parkingLocker)
            {
                if (sum < 0)
                {
                    throw new ArgumentException("Sum can't be negative!");
                }

                if (!_parking.Vehicles.Exists(v => v.Id == vehicleId))
                {
                    throw new ArgumentException("No such vehicle in the parking!");
                }

                _parking.Vehicles.Find(v => v.Id == vehicleId).Balance += sum;
            }
        }

        public bool Exists(string vehicleID)
        {
            return _parking.Vehicles.Exists(v => v.Id == vehicleID);
        }

        private void DoTransaction(object sender, ElapsedEventArgs e)
        {
            lock (parkingLocker)
            {
                foreach (var vehicle in _parking.Vehicles)
                {
                    var price = Settings.Prices[vehicle.VehicleType];

                    if (vehicle.Balance < Settings.Prices[vehicle.VehicleType] && vehicle.Balance > 0)
                    {
                        price = vehicle.Balance + ((Settings.Prices[vehicle.VehicleType] - vehicle.Balance) * Settings.FineCoefficient);
                    }
                    else if (vehicle.Balance <= 0)
                    {
                        price *= Settings.FineCoefficient;
                    }

                    vehicle.Balance -= price;
                    _parking.Balance += price;
                    transactions.Add(new TransactionInfo(vehicle.Id, price));
                }
            }
        }

        private void Log(object sender, ElapsedEventArgs e)
        {
            lock (parkingLocker)
            {
                var stringBuilder = new StringBuilder();

                transactions.ForEach(item => stringBuilder.Append(item.ToString()));

                _logService.Write(stringBuilder.ToString());
                transactions.Clear();
            }
        }

        public void Dispose()
        {    
            _parking.Vehicles.Clear();
            _parking.Balance = 0;
            _logTimer.Stop();
            _withdrawTimer.Stop();
            File.Delete(Settings.LogPath);
        }
    }
}