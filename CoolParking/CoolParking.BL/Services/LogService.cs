﻿using System;
using System.IO;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        string logFilePath;

        public LogService(string logFilePath)
        {
            this.logFilePath = logFilePath;
        }

        public string LogPath => logFilePath;

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new InvalidOperationException("File not found. Logging are happening every minute.");
            }
            using(StreamReader file = new StreamReader(LogPath))
            {
                return file.ReadToEnd();
            }
        }

        public void Write(string logInfo)
        {
            using (StreamWriter file = new StreamWriter(LogPath, true))
            {
                file.WriteLine(logInfo);
            }
        }
    }
}