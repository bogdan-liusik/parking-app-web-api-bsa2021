﻿using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        Timer _timer;

        public TimerService(double interval) 
        {
            Interval = interval;
            _timer = new Timer();
            _timer.Interval = Interval;
        }

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void Start()
        {
            _timer.Elapsed += Elapsed;
            _timer.Enabled = true;
        }

        public void Stop()
        {
            _timer.Enabled = false;
        }

        public void Dispose()
        {
            _timer.Dispose();
        }
    }
}