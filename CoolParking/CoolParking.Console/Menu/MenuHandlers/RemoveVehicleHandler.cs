﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using System.Net.Http;
    using static MenuHelper;

    class RemoveVehicleHandler : MenuHandler
    {
        public RemoveVehicleHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                new DisplayVehiclesInParkingHandler(HttpClient).Handle();
                Console.Write("To remove vehicle enter ID: ");
                string ID = Console.ReadLine();

                var task = HttpClient.DeleteAsync($"https://localhost:44322/api/vehicles/{ID}").Result;

                if (!task.IsSuccessStatusCode)
                {
                    WriteErrorLine(task.StatusCode.ToString());
                    return;
                }

                WriteSuccessLine("Successfully removed.");
                WriteEnterAnyKeyToContinue();
            }
            catch(AggregateException exception)
            {
                WriteErrorLine(exception.Message);
            }
            catch (Exception exception)
            {
                WriteErrorLine(exception.Message);
            }
        }
    }
}