﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using Newtonsoft.Json;
    using System.Net.Http;
    using CoolParking.BL.Models;
    using System.Collections.Generic;
    using static MenuHelper;

    class DisplayVehiclesInParkingHandler : MenuHandler
    {
        public DisplayVehiclesInParkingHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                List<VehicleModel> vehicles = null;
                var task = HttpClient.GetAsync("https://localhost:44322/api/vehicles")
                .ContinueWith(t =>
                {
                    var response = t.Result;
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    vehicles = JsonConvert.DeserializeObject<List<VehicleModel>>(jsonString.Result);
                });
                task.Wait();

                foreach (var item in vehicles)
                {
                    WriteColorLine($"ID: {item.Id} | Type: {item.VehicleType} with balance {item.Balance} y.o", ConsoleColor.Yellow);
                }
            }
            catch (AggregateException ex)
            {
                WriteErrorLine(ex.Message);
            }
            catch (Exception ex)
            {
                WriteErrorLine(ex.Message);
            }
        }
    }
}