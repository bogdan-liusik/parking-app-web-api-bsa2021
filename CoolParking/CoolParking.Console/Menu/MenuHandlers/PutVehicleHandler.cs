﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using System.Net.Http;
    using Newtonsoft.Json;
    using System.Text;
    using CoolParking.BL.Models;
    using static MenuHelper;

    class PutVehicleHandler : MenuHandler
    {
        public PutVehicleHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                int type;
                decimal balance;

                Console.WriteLine($"1. {VehicleType.PassengerCar}\n2. {VehicleType.Truck}\n3. {VehicleType.Bus}\n4. {VehicleType.Motorcycle}");
                Console.Write("Please, choose vehicle type (above): ");

                Int32.TryParse(Console.ReadLine(), out type);

                if (type > 4 || type < 1)
                {
                    Console.WriteLine("Type must be in range 1 - 4, try again.");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                VehicleType vehicleType = (VehicleType)type;

                Console.Write($"Enter balance (greater than {Settings.Prices[vehicleType]} y.e  for such type of the vehicle): ");
                Decimal.TryParse(Console.ReadLine(), out balance);

                if (balance < Settings.Prices[vehicleType])
                {
                    Console.WriteLine($"Balance must be greater than {Settings.Prices[vehicleType]} y.e");
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                var vehicle = new Vehicle(vehicleType, balance);

                var vehicleContent = JsonConvert.SerializeObject(vehicle);
                var requestContent = new StringContent(vehicleContent, Encoding.UTF8, "application/json");

                var result = HttpClient.PostAsync("https://localhost:44322/api/vehicles", requestContent).Result;

                if (!result.IsSuccessStatusCode)
                {
                    WriteErrorLine(result.StatusCode.ToString());
                    WriteEnterAnyKeyToContinue();
                    return;
                }

                WriteSuccessLine($"Successfully added with ID {vehicle.Id}");
                WriteEnterAnyKeyToContinue();
            }
            catch(ArgumentException exception)
            {
                WriteErrorLine(exception.Message);
            }
            catch(InvalidOperationException exception)
            {
                WriteErrorLine(exception.Message);
            }
            catch (Exception exception)
            {
                WriteErrorLine(exception.Message);
            }
        }
    }
}