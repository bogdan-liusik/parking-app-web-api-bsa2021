﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using System.Net.Http;
    using static MenuHelper;

    class DisplayTransactionHistoryHandler : MenuHandler
    {
        public DisplayTransactionHistoryHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                WriteColorLine(HttpClient.GetStringAsync("https://localhost:44322/api/transactions/all").Result, System.ConsoleColor.Yellow);
            }
            catch (AggregateException ex)
            {
                WriteErrorLine(ex.Message);
            }
            catch (Exception ex)
            {
                WriteErrorLine(ex.Message);
            }
        }
    }
}