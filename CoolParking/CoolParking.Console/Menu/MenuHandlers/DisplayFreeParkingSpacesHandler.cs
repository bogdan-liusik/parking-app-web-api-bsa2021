﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System.Net.Http;
    using static MenuHelper;

    class DisplayFreeParkingSpacesHandler : MenuHandler
    {
        public DisplayFreeParkingSpacesHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                var task = HttpClient.GetAsync("https://localhost:44322/api/parking/freePlaces")
                .ContinueWith(t =>
                {
                    var response = t.Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        WriteErrorLine($"Status code: {response.StatusCode}");
                        return;
                    }

                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    WriteSuccessLine($"Now are available {jsonString.Result} parking spaces.");
                    WriteEnterAnyKeyToContinue();
                });
                task.Wait();
            }
            catch (System.Exception ex)
            {
                WriteErrorLine(ex.Message);
            }
        }
    }
}