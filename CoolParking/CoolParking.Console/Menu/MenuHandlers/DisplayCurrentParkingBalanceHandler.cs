﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System.Net.Http;
    using static MenuHelper;

    class DisplayCurrentParkingBalanceHandler : MenuHandler
    {
        public DisplayCurrentParkingBalanceHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                var task = HttpClient.GetAsync("https://localhost:44322/api/parking/balance")
                .ContinueWith(t =>
                {
                    var response = t.Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        WriteErrorLine($"Status code: {response.StatusCode}");
                        return;
                    }

                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    WriteSuccessLine($"Current parking balance: {jsonString.Result} y.e");
                    WriteEnterAnyKeyToContinue();
                });
                task.Wait();
            }
            catch (System.Exception ex)
            {
                WriteErrorLine(ex.Message);
            }    
        }
    }
}