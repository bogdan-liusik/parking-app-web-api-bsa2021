﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System.Net.Http;
    using static MenuHelper;

    class DisplayEarnedMoneyHandler : MenuHandler
    {
        public DisplayEarnedMoneyHandler(HttpClient httpClient) : base(httpClient) { }
        public override void Handle()
        {
            try
            {
                var task = HttpClient.GetAsync("https://localhost:44322/api/transactions/lastEarnedMoney")
                .ContinueWith(t =>
                {
                    var response = t.Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        WriteErrorLine($"Status code: {response.StatusCode}");
                        return;
                    }

                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    WriteSuccessLine($"Sum of earned money for the current period (before logging): {jsonString.Result} y.e");
                    WriteEnterAnyKeyToContinue();
                });
                task.Wait();
            }
            catch (System.Exception ex)
            {
                WriteErrorLine(ex.Message);
            }
        }
    }
}