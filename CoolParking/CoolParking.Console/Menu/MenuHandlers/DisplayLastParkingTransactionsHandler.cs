﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using System.Net.Http;
    using Newtonsoft.Json;
    using CoolParking.BL.Models;
    using System.Collections.Generic;
    using static MenuHelper;

    class DisplayLastParkingTransactionsHandler : MenuHandler
    {
        public DisplayLastParkingTransactionsHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                List<TransactionInfo> vehicles = null;
                var task = HttpClient.GetAsync("https://localhost:44322/api/transactions/last")
                .ContinueWith(t =>
                {
                    var response = t.Result;
                    var jsonString = response.Content.ReadAsStringAsync();
                    jsonString.Wait();
                    vehicles = JsonConvert.DeserializeObject<List<TransactionInfo>>(jsonString.Result);
                });
                task.Wait();

                foreach (var item in vehicles)
                {
                    WriteColorLine(item.ToString(), System.ConsoleColor.Yellow);
                }
            }
            catch (AggregateException ex)
            {
                WriteErrorLine(ex.Message);
            }
            catch (Exception ex)
            {
                WriteErrorLine(ex.Message);
            }
        }
    }
}