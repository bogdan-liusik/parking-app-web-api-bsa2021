﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System;
    using System.Text;
    using Newtonsoft.Json;
    using System.Net.Http;
    using static MenuHelper;
    using CoolParking.BL.Models;

    class RefillVehicleBalanceHandler : MenuHandler
    {
        public RefillVehicleBalanceHandler(HttpClient httpClient) : base(httpClient) { }

        public override void Handle()
        {
            try
            {
                new DisplayVehiclesInParkingHandler(HttpClient).Handle();
                Console.Write("Enter vehicle ID for which you want to change balance: ");
                string ID = Console.ReadLine();

                Console.Write("Enter the amount you want to top up your balance: ");
                decimal sum;
                Decimal.TryParse(Console.ReadLine(), out sum);

                var put = new TopUpVehicleModel()
                {
                    Id = ID,
                    Sum = sum
                };

                var serializedPut = JsonConvert.SerializeObject(put);
                var requestContent = new StringContent(serializedPut, Encoding.UTF8, "application/json");

                var result = HttpClient.PutAsync($"https://localhost:44322/api/transactions/topUpVehicle", requestContent).Result;

                if (!result.IsSuccessStatusCode)
                {
                    WriteErrorLine(result.StatusCode.ToString());
                    return;
                }

                WriteSuccessLine("Successfully top upped.");
                WriteEnterAnyKeyToContinue();
            }
            catch(InvalidOperationException exception)
            {
                WriteErrorLine(exception.Message);
            }
            catch(ArgumentException exception)
            {
                WriteErrorLine(exception.Message);
            }
            catch (Exception exception)
            {
                WriteErrorLine(exception.Message);
            }
        }
    }
}