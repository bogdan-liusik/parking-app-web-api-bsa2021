﻿namespace CoolParking.BL.Console
{
    using System;
    using MenuHandlers;
    using CoolParking.BL.Services;
    using System.Collections.Generic;
    using static MenuHelper;
    using System.Net.Http;
    using System.Threading;

    class Menu
    {
        private bool running = true;

        Dictionary<int, MenuHandler> handlers;
        HttpClient _httpClient;

        public Menu(HttpClient httpClient)
        {
            _httpClient = httpClient;
            
            handlers = new Dictionary<int, MenuHandler>()
            {
                {1, new DisplayCurrentParkingBalanceHandler(_httpClient)},
                {2, new DisplayEarnedMoneyHandler(_httpClient)},
                {3, new DisplayFreeParkingSpacesHandler(_httpClient)},
                {4, new DisplayLastParkingTransactionsHandler(_httpClient)},
                {5, new DisplayTransactionHistoryHandler(_httpClient)},
                {6, new DisplayVehiclesInParkingHandler(_httpClient)},
                {7, new PutVehicleHandler(_httpClient)},
                {8, new RemoveVehicleHandler(_httpClient)},
                {9, new RefillVehicleBalanceHandler(_httpClient)}
            };
        }

        public void Start()
        {
            while (running)
            {
                ShowCommands();
                ChooseCommand();
            }
        }
        
        private void ShowCommands()
        {
            WriteColorLine("┌" + new string('─', 33) + "PARKING MENU" + new string('─', 35) + "┐", ConsoleColor.Green);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("| 1. Display the current parking balance.                                        |");
            Console.WriteLine("| 2. Display the amount of earned money for the current period (before logging). |");
            Console.WriteLine("| 3. Display the number of free parking spaces.                                  |");
            Console.WriteLine("| 4. Display all parking transactions for the current period (before logging).   |");
            Console.WriteLine("| 5. Display the transaction history on the screen.                              |");
            Console.WriteLine("| 6. Display a list of vehicles in the parking.                                  |");
            Console.WriteLine("| 7. Put the vehicle in the parking lot.                                         |");
            Console.WriteLine("| 8. Remove vehicle from parking by ID.                                          |");
            Console.WriteLine("| 9. Refill the balance of a particular vehicle.                                 |");
            WriteColorLine("| 0. Exit                                                                        |", ConsoleColor.Red);
            WriteColorLine("└" + new string('─', 80) + "┘", ConsoleColor.Green);
        }

        private void ChooseCommand()
        {
            try
            {
                int menuItem;
                Console.Write("Enter your choice: ");
                
                if(Int32.TryParse(Console.ReadLine(), out menuItem))
                {
                    if (menuItem < 10 && menuItem > 0 && menuItem != 0)
                    {
                        handlers[menuItem].Handle();
                    }
                    else if(menuItem == 0)
                    {
                        running = false;
                    }
                    else
                    {
                        throw new InvalidOperationException("Item number must be in range 0 - 9! Try Again.");
                    }
                }
                else
                {
                    throw new InvalidOperationException("Invalid character! Try Again.");
                }
            }
            catch (InvalidOperationException ex)
            {
                WriteErrorLine(ex.Message);
            }

            WriteColorLine("\n" + new string('─', 81) + "\n", ConsoleColor.DarkGray);
        }
    }
}
