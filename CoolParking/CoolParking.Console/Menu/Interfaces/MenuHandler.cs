﻿namespace CoolParking.BL.Console.MenuHandlers
{
    using System.Net.Http;

    abstract class MenuHandler
    {
        HttpClient _httpClient;

        public MenuHandler(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public HttpClient HttpClient => _httpClient;

        public abstract void Handle();
    }
}