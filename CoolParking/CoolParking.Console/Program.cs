﻿namespace CoolParking.BL.Console
{
    using System.Net.Http;

    class Program
    {
        static void Main(string[] args)
        {
            using (var httpClient = new HttpClient())
            {
                new Menu(httpClient).Start();
            }
        }
    }
}