﻿using System;
using System.Linq;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Models;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public class TransactionsController : ControllerBase
    {
        readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        public IActionResult GetLastTransactions()
        {
            return Ok(_parkingService.GetLastParkingTransactions());
        }

        [HttpGet("lastEarnedMoney")]
        public IActionResult GetLastEarnedMoney()
        {
            return Ok(_parkingService.GetLastParkingTransactions().Sum(t => t.Sum));
        }

        [HttpGet("all")]
        public IActionResult GetAllTransactions()
        {
            try
            {
                return Ok(_parkingService.ReadFromLog());
            }
            catch (InvalidOperationException ex)
            {
                return NotFound(ex.Message);
            }
        }

        [HttpPut("topUpVehicle")]
        public IActionResult PutTopUpVehicle(TopUpVehicleModel model)
        {
            try
            { 
                if (!Vehicle.IsValidateID(model.Id))
                {
                    return BadRequest("Id is not valid");
                }
                _parkingService.TopUpVehicle(model.Id, model.Sum);
                return Ok(_parkingService.GetVehicles().Where(v => v.Id == model.Id).First());
            }
            catch (ArgumentException ex)
            {
                if(ex.Message == "Sum can't be negative!")
                {
                    return BadRequest(ex.Message);
                }
                return NotFound(ex.Message);
            }
        }
    }
}