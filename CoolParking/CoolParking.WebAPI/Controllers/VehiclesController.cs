﻿using System;
using System.Linq;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/vehicles")]
    public class VehiclesController : ControllerBase
    {
        readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        public IActionResult GetVehicles()
        {
            return Ok(_parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        public IActionResult GetVehicleByID(string id)
        {
            if (!Vehicle.IsValidateID(id))
            {
                return BadRequest("Id is not valid.");
            }
            var vehicle = _parkingService.GetVehicles().Where(v => v.Id == id).FirstOrDefault();
            if(vehicle == null)
            {
                return NotFound("No such car in the parking.");
            }
            return Ok(vehicle);
        }

        [HttpPost]
        public IActionResult PostVehicle(VehicleModel vehicle)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Body is invalid");
            }

            try
            {
                var _vehicle = new Vehicle(vehicle.Id, vehicle.VehicleType, vehicle.Balance);
                _parkingService.AddVehicle(_vehicle);

                var host = HttpContext.Request.Host;
                var path = HttpContext.Request.Path;
                var schema = HttpContext.Request.Scheme;

                return Created(new Uri($"{schema}://{host.Value}{path.Value}"), _vehicle);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch(ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteVehicleByID(string id)
        {
            if (!Vehicle.IsValidateID(id))
            {
                return BadRequest("Id is not valid.");
            }
            try
            {
                _parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}