﻿using Microsoft.AspNetCore.Mvc;
using CoolParking.BL.Interfaces;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/parking")]
    public class ParkingController : ControllerBase
    {
        readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        public IActionResult GeParkingBalance()
        {
            return Ok(_parkingService.GetBalance());
        }

        [HttpGet("capacity")]
        public IActionResult GetParkingCapacity()
        {
            return Ok(_parkingService.GetCapacity());
        }

        [HttpGet("freePlaces")]
        public IActionResult GetParkingFreeSpaces()
        {
            return Ok(_parkingService.GetFreePlaces());
        }
    }
}